﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Extend.Inventory
{
    /// <summary>This class represents an inventory file.</summary>
    internal sealed class __InvFile
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fileName">File name.</param>
        /// <param name="hash">Hash value.</param>
        internal __InvFile(string fileName, string hash)
        {
            FileName = fileName;
            CurrentHash = hash;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Data.</param>
        internal __InvFile(string data)
        {
            FileName = data.Substring(22);
            SavedHash = data.Substring(0, 22);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the file name.</summary>
        public string FileName
        {
            get; set;
        }


        /// <summary>Gets or sets the current hash.</summary>
        public string CurrentHash
        {
            get; set;
        }


        /// <summary>Gets or sets the hash saved in the inventory.</summary>
        public string SavedHash
        {
            get; set;
        }
    }
}
