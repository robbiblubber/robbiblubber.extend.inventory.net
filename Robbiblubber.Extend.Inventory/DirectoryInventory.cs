﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;



namespace Robbiblubber.Extend.Inventory
{
    /// <summary>This class represents an inventory directory.</summary>
    public sealed class DirectoryInventory
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>MD5 algorithm.</summary>
        private static MD5 _Md5 = MD5.Create();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal members                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Files.</summary>
        internal Dictionary<string, __InvFile> _Files = null;

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="directoryName"></param>
        /// <param name="recursive">Determines if the directory is inventoried recursively.</param>
        public DirectoryInventory(string directoryName, bool recursive)
        {
            _Read(directoryName, false, recursive);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="directoryName"></param>
        public DirectoryInventory(string directoryName)
        {
            _Read(directoryName, true, true);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the directory name.</summary>
        public string DirectoryName
        {
            get; private set;
        }


        /// <summary>Gets if the directory is inventoryied recursively.</summary>
        public bool IsRecursive
        {
            get; private set;
        }


        /// <summary>Gets all changed files.</summary>
        public string[] ChangedFiles
        {
            get { return GetChangedFiles(true); }
        }


        /// <summary>Gets all new files.</summary>
        public string[] NewFiles
        {
            get { return GetNewFiles(true); }
        }


        /// <summary>Gets all deleted files.</summary>
        public string[] DeletedFiles
        {
            get { return GetDeletedFiles(true); }
        }


        /// <summary>Gets all unchanged files.</summary>
        /// <remarks>Unchanged files have not been changed since the last time the inventory was saved.</remarks>
        public string[] UnchangedFiles
        {
            get { return GetUnchangedFiles(true); }
        }


        /// <summary>Gets all changed or new files.</summary>
        public string[] ChangedOrNewFiles
        {
            get
            {
                List<string> rval = new List<string>();
                foreach(__InvFile i in _Files.Values)
                {
                    if((i.CurrentHash != i.SavedHash) && (i.CurrentHash != null)) { rval.Add(DirectoryName + @"\" + i.FileName); }
                }

                return rval.ToArray();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshs the directory.</summary>
        public void Refresh()
        {
            _Refresh(DirectoryName);
        }


        /// <summary>Updates the directory and discards changes.</summary>
        public void Update()
        {
            if(File.Exists(_InventoryFile)) { File.Delete(_InventoryFile); }

            using(FileStream inv = File.OpenWrite(_InventoryFile))
            {
                using(GZipStream zip = new GZipStream(inv, CompressionMode.Compress))
                {
                    using(var wr = new StreamWriter(zip))
                    {
                        wr.WriteLine(":inv/1.0");
                        if(IsRecursive) { wr.WriteLine(":recursive"); }

                        foreach(__InvFile i in _Files.Values)
                        {
                            if(i.CurrentHash != null) { wr.WriteLine(i.CurrentHash + i.FileName); }
                        }
                    }
                }
            }
        }


        /// <summary>Copies all changed and new files to a target directory.</summary>
        /// <param name="targetDirectory">Target directory.</param>
        /// <remarks>This method maintains the original directory structure in the target directory.</remarks>
        public void CopyChangedAndNewFilesTo(string targetDirectory)
        {
            foreach(__InvFile i in _Files.Values)
            {
                if((i.CurrentHash != i.SavedHash) && (i.CurrentHash != null))
                {
                    if(!Directory.Exists(targetDirectory + @"\" + Path.GetDirectoryName(i.FileName)))
                    {
                        Directory.CreateDirectory(targetDirectory + @"\" + Path.GetDirectoryName(i.FileName));
                    }

                    if(File.Exists(targetDirectory + @"\" + i.FileName)) { File.Delete(targetDirectory + @"\" + i.FileName); }
                    File.Copy(DirectoryName + @"\" + i.FileName, targetDirectory + @"\" + i.FileName);
                }
            }
        }


        /// <summary>Gets all changed file names.</summary>
        /// <param name="includeDirectoryName">Include directory name in results.</param>
        /// <remarks>Changed files do not include new or deleted files.</remarks>
        public string[] GetChangedFiles(bool includeDirectoryName = false)
        {
            List<string> rval = new List<string>();
            string prefix = (includeDirectoryName ? DirectoryName + @"\" : "");
            
            foreach(__InvFile i in _Files.Values)
            {
                if((i.CurrentHash != i.SavedHash) && (i.CurrentHash != null) && (i.SavedHash != null)) { rval.Add(prefix + i.FileName); }
            }

            return rval.ToArray();
        }


        /// <summary>Gets all new file names.</summary>
        /// <param name="includeDirectoryName">Include directory name in results.</param>
        /// <remarks>New files are files that had not been present the last time the inventory was saved.</remarks>
        public string[] GetNewFiles(bool includeDirectoryName = false)
        {
            List<string> rval = new List<string>();
            string prefix = (includeDirectoryName ? DirectoryName + @"\" : "");

            foreach(__InvFile i in _Files.Values)
            {
                if(i.SavedHash == null) { rval.Add(prefix + i.FileName); }
            }

            return rval.ToArray();
        }


        /// <summary>Gets all deleted files.</summary>
        /// <remarks>Deleted files have been saved in the last inventory but are not present in the directory now.</remarks>
        public string[] GetDeletedFiles(bool includeDirectoryName = false)
        {
            List<string> rval = new List<string>();
            string prefix = (includeDirectoryName ? DirectoryName + @"\" : "");

            foreach(__InvFile i in _Files.Values)
            {
                if(i.CurrentHash == null) { rval.Add(prefix + i.FileName); }
            }

            return rval.ToArray();
        }


        /// <summary>Gets all unchanged files.</summary>
        /// <remarks>Unchanged files have not been changed since the last time the inventory was saved.</remarks>
        public string[] GetUnchangedFiles(bool includeDirectoryName = false)
        {
            List<string> rval = new List<string>();
            string prefix = (includeDirectoryName ? DirectoryName + @"\" : "");

            foreach(__InvFile i in _Files.Values)
            {
                if(i.CurrentHash == i.SavedHash) { rval.Add(prefix + i.FileName); }
            }

            return rval.ToArray();
        }


        /// <summary>Copies all changed files to a directory.</summary>
        /// <param name="targetDirectory">Target directory</param>
        public int CopyChangedFiles(string targetDirectory)
        {
            return _CopyFiles(GetChangedFiles(), targetDirectory);
        }


        /// <summary>Copies all changed files to a directory.</summary>
        /// <param name="targetDirectory">Target directory</param>
        public int CopyNewFiles(string targetDirectory)
        {
            return _CopyFiles(GetNewFiles(), targetDirectory);
        }


        /// <summary>Copies all changed files to a directory.</summary>
        /// <param name="targetDirectory">Target directory</param>
        public int CopyUnchangedFiles(string targetDirectory)
        {
            return _CopyFiles(GetUnchangedFiles(), targetDirectory);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Computes a MD5 hash for a file.</summary>
        /// <param name="fileName">File name.</param>
        /// <returns>Hash string.</returns>
        internal static string _Hash(string fileName)
        {
            using(FileStream stream = File.OpenRead(fileName))
            {
                return Convert.ToBase64String(_Md5.ComputeHash(stream)).TrimEnd('=');
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private properties                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the inventory file name.</summary>
        private string _InventoryFile
        {
            get { return DirectoryName + @"\.inventory"; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Copies a set of files to a directory.</summary>
        /// <param name="files">Array of file names.</param>
        /// <param name="targetDirectory">Target directory.</param>
        private int _CopyFiles(string[] files, string targetDirectory)
        {
            targetDirectory = Path.GetFullPath(targetDirectory);
            if(!Directory.Exists(targetDirectory)) { Directory.CreateDirectory(targetDirectory); }

            foreach(string i in files)
            {
                if(!string.IsNullOrWhiteSpace(Path.GetDirectoryName(i)))
                {
                    if(!Directory.Exists(targetDirectory + @"\" + Path.GetDirectoryName(i))) { Directory.CreateDirectory(targetDirectory + @"\" + Path.GetDirectoryName(i)); }
                }

                File.Copy(DirectoryName + @"\" + i, targetDirectory + @"\" + i, true);
            }

            return files.Length;
        }


        /// <summary>Reads data for this instance with directory data.</summary>
        /// <param name="directoryName">Directory name.</param>
        /// <param name="allowSetRecursive">Determines if the recursive flag may be set set automatically.</param>
        /// <param name="recursive">Determines if the directory is inventoried recursively.</param>
        private void _Read(string directoryName, bool allowSetRecursive, bool recursive)
        {
            DirectoryName = Path.GetFullPath(directoryName);

            if(!Directory.Exists(DirectoryName)) { throw new DirectoryNotFoundException(); }

            _Files = new Dictionary<string, __InvFile>();
            if(File.Exists(_InventoryFile))
            {
                __InvFile f;
                string l;

                using(FileStream inv = File.OpenRead(_InventoryFile))
                {
                    using(GZipStream zip = new GZipStream(inv, CompressionMode.Decompress))
                    {
                        using(var re = new StreamReader(zip))
                        {
                            while((l = re.ReadLine()) != null)
                            {
                                if(l == ":recursive")
                                {
                                    IsRecursive = true;
                                    continue;
                                }
                                else if(string.IsNullOrWhiteSpace(l) || l.StartsWith(":")) continue;

                                f = new __InvFile(l);
                                _Files.Add(f.FileName, f);
                            }
                        }
                    }
                }
            }
            else
            {
                IsRecursive = recursive;
            }

            if((!allowSetRecursive) && (recursive != IsRecursive)) { throw new InvalidOperationException("Invalid recursivity option."); }
            Refresh();
        }


        /// <summary>Updates a directory.</summary>
        /// <param name="directoryName">Directory name.</param>
        private void _Refresh(string directoryName)
        {
            string fileName;
            foreach(string i in Directory.GetFiles(directoryName))
            {
                if(i == _InventoryFile) continue;
                fileName = i.Substring(DirectoryName.Length + 1);
                
                if(_Files.ContainsKey(fileName))
                {
                    _Files[fileName].CurrentHash = _Hash(i);
                }
                else { _Files.Add(fileName, new __InvFile(fileName, _Hash(i))); }
            }

            if(IsRecursive)
            {
                foreach(string i in Directory.GetDirectories(directoryName))
                {
                    _Refresh(i);
                }
            }
        }
    }
}
