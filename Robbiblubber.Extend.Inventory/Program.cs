﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Extend.Inventory
{
    internal static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // main entry point                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        /// <param name="args">Arguments.</param>
        internal static void Main(string[] args)
        {
            if(args.Length == 0)
            {
                Console.WriteLine("dvtry: Missing arguments.");
                return;
            }


            if((args[0] == "-?") || (args[0] == "/?") || (args[0] == "--help"))
            {
                Assembly a = Assembly.GetEntryAssembly();
                if(a == null) { a = Assembly.GetCallingAssembly(); }

                string usageFile = Path.GetDirectoryName(a.Location) + @"\dvtry.usage";

                if(File.Exists(usageFile))
                {
                    foreach(string i in File.ReadAllLines(usageFile)) { Console.WriteLine(i); }
                }
                else { Console.WriteLine("dvtry: Help text unavailable."); }

                return;
            }


            if(args[0].StartsWith("-i") || args[0].StartsWith("/i") || (args[0] == "--init"))
            {
                bool recursive = args[0].Contains("r");
                bool notrecursive = args[0].Contains("N");

                string dir = null;

                for(int i = 1; i < args.Length; i++)
                {
                    if((args[i] == "-r") || (args[i] == "/r") || (args[i] == "--recursive"))
                    {
                        recursive = true;
                    }
                    else if((args[i] == "-N") || (args[i] == "/N") || (args[i] == "--not-recursive"))
                    {
                        notrecursive = true;
                    }
                    else
                    {
                        if(dir != null) { Console.WriteLine("dvtry: Invalid arguments."); return; }

                        dir = args[i];
                    }
                }

                if(recursive && notrecursive) { Console.WriteLine("dvtry: Inconsistent arguments."); return; }

                DirectoryInventory inv;
                try
                {
                    inv = new DirectoryInventory(dir, !notrecursive);
                }
                catch(InvalidOperationException ex) { Console.WriteLine("dvtry: " + ex.Message); return; }
                catch(Exception) { Console.WriteLine("dvtry: Invalid directory."); return; }

                try
                {
                    inv.Update();

                    Console.WriteLine("Directory updated.");
                    return;
                }
                catch(Exception) { Console.WriteLine("dvtry: Failed to update."); return; }
            }


            if((args[0] == "-u") || (args[0] == "/u") || (args[0] == "--update"))
            {
                if(args.Length != 2) { Console.WriteLine("dvtry: Invalid arguments."); return; }

                DirectoryInventory inv;
                try
                {
                    inv = new DirectoryInventory(args[1]);
                }
                catch(Exception) { Console.WriteLine("dvtry: Invalid directory."); return; }

                try
                {
                    inv.Update();

                    Console.WriteLine("Directory updated.");
                    return;
                }
                catch(Exception) { Console.WriteLine("dvtry: Failed to update."); return; }
            }


            if(args[0].StartsWith("-l") || args[0].StartsWith("/l") || (args[0] == "--list"))
            {
                bool lchanged = args[0].Contains("c");
                bool ldeleted = args[0].Contains("d");
                bool lunchanged = (args[0].Contains("g") || args[0].Contains("u"));
                bool lnew = args[0].Contains("n");

                bool print = args[0].Contains("p");
                bool xdir = args[0].Contains("x");

                string dir = null;
                
                for(int i = 1; i < args.Length; i++)
                {
                    if((args[i] == "-c") || (args[i] == "/c") || (args[i] == "--changed"))
                    {
                        lchanged = true;
                    }
                    else if((args[i] == "-d") || (args[i] == "/d") || (args[i] == "--deleted"))
                    {
                        ldeleted = true;
                    }
                    else if((args[i] == "-g") || (args[i] == "/g") || (args[i] == "-u") || (args[i] == "/u") || (args[i] == "--unchanged"))
                    {
                        lunchanged = true;
                    }
                    else if((args[i] == "-n") || (args[i] == "/n") || (args[i] == "--new"))
                    {
                        lnew = true;
                    }
                    else if((args[i] == "-p") || (args[i] == "/p") || (args[i] == "--print"))
                    {
                        print = true;
                    }
                    else if((args[i] == "-x") || (args[i] == "/x") || (args[i] == "--nodirname"))
                    {
                        xdir = true;
                    }
                    else
                    {
                        if(dir != null) { Console.WriteLine("dvtry: Invalid arguments."); return; }

                        dir = args[i];
                    }
                }

                DirectoryInventory inv;
                try
                {
                    inv = new DirectoryInventory(dir);
                }
                catch(Exception) { Console.WriteLine("dvtry: Invalid directory."); return; }

                string p = null;
                if(lchanged)   { if(print) p = "c "; foreach(string i in inv.GetChangedFiles(!xdir)) { Console.WriteLine(p + i); } }
                if(ldeleted)   { if(print) p = "d "; foreach(string i in inv.GetDeletedFiles(!xdir)) { Console.WriteLine(p + i); } }
                if(lunchanged) { if(print) p = "u "; foreach(string i in inv.GetUnchangedFiles(!xdir)) { Console.WriteLine(p + i); } }
                if(lnew)       { if(print) p = "n "; foreach(string i in inv.GetNewFiles(!xdir)) { Console.WriteLine(p + i); } }
                return;
            }


            if(args[0].StartsWith("-y") || args[0].StartsWith("/y") || (args[0] == "--copy"))
            {
                bool lchanged = args[0].Contains("c");
                bool lunchanged = (args[0].Contains("g") || args[0].Contains("u"));
                bool lnew = args[0].Contains("n");

                string dir = null;
                string tar = null;

                for(int i = 1; i < args.Length; i++)
                {
                    if((args[i] == "-c") || (args[i] == "/c") || (args[i] == "--changed"))
                    {
                        lchanged = true;
                    }
                    else if((args[i] == "-g") || (args[i] == "/g") || (args[i] == "-u") || (args[i] == "/u") || (args[i] == "--unchanged"))
                    {
                        lunchanged = true;
                    }
                    else if((args[i] == "-n") || (args[i] == "/n") || (args[i] == "--new"))
                    {
                        lnew = true;
                    }
                    else
                    {
                        if(dir == null)
                        {
                            dir = args[i];
                        }
                        else if(tar == null)
                        {
                            tar = args[i];
                        }
                        else { Console.WriteLine("dvtry: Invalid arguments."); return; }
                    }
                }

                if((dir == null) || (tar == null)) { Console.WriteLine("dvtry: Invalid arguments."); return; }

                DirectoryInventory inv;
                try
                {
                    inv = new DirectoryInventory(dir);
                }
                catch(Exception) { Console.WriteLine("dvtry: Invalid directory."); return; }

                int n = 0;
                try
                {
                    if(lchanged) { n += inv.CopyChangedFiles(tar); }
                    if(lunchanged) { n += inv.CopyUnchangedFiles(tar); }
                    if(lnew) { n += inv.CopyNewFiles(tar); }
                }
                catch(Exception) { Console.WriteLine("dvtry: Copy failed."); return; }

                Console.WriteLine(n.ToString() + " files copied.");
                return;
            }
        }
    }
}
